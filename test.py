from random import randint
input("choose a number between 1 and 50 (press enter to start)")
#We need to let it know what we are guessing

num_guesses = input("How many guesses should the computer get?\n")
num_guesses = int(num_guesses)

#Figure out a range of guesses

low = 1
high = 50

guesses = []

for guess_num in range(num_guesses):
    
    guess = randint (low, high) #int((low+high)/2) #would we change this to the randint function with the range of low,high
    
    
    print("\n Is", guess, "your number? (please respond with higher, lower, or correct)")
    answer = input("Answer: ")
    
    #save the current guesses and answers given into the list guesses
    guesses.append([guess, answer])

    if answer == "correct":
        print(" \n WHOOHOO! The computer correctly guessed your number:", guess)
        exit()
    elif answer == "lower":
        high = guess - 1
        print("Shoot! let me try again!")
            #need to generate the next random number with the new low and high range
    elif answer == "higher":
        low = guess + 1
        print("Darn! Let me try again!")
    else:
        print("Bad input, try again!")
print("/nThe computer ran out of guesses, you win!")
print("Here's all the numbers the computer guessed")

for guess in guesses:
    print(guess)